﻿using UnityEngine;
using System.Collections;

public class PopUpVolume : MonoBehaviour 
{
	public bool ActiveAtStart = false;

	float oldGravityScale;
	GameObject player = null;

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag("Player") == false)
			return;

		oldGravityScale = other.rigidbody2D.gravityScale;
		other.rigidbody2D.gravityScale = 0.4f;

		PlayerController pc = other.GetComponent<PlayerController> ();
		if(pc == null)
			return;

		pc.inPopup = true;
		player = other.gameObject;
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if(other.CompareTag("Player") == false)
			return;
		other.rigidbody2D.gravityScale = oldGravityScale;

		PlayerController pc = other.GetComponent<PlayerController> ();
		if(pc == null)
			return;
		
		pc.inPopup = false;
	}

	void OnDisable()
	{
		Reset ();
	}

	void OnDestroy()
	{
		Reset ();
	}

	void Reset()
	{
		if (player == null)
						return;

		if(player.CompareTag("Player") == false)
			return;
		player.rigidbody2D.gravityScale = oldGravityScale;
		
		PlayerController pc = player.GetComponent<PlayerController> ();
		if(pc == null)
			return;
		
		pc.inPopup = false;
		player = null;
	}

	// Use this for initialization
	void Start () 
	{
		gameObject.SetActive (ActiveAtStart);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
