﻿using UnityEngine;
using System.Collections;

public class BrowserTab : MonoBehaviour 
{
	Browser owner = null;

	string tabName = "";
	public string TabName {
		get {
			return tabName;
		}
		set {
			tabName = value;
		}
	}

	Vector2 position;

	bool isActive = false;
	public bool IsActive {
		get {
			return isActive;
		}
	}

	int index = 0;

	public int Index {
		get {
			return index;
		}
		set {
			index = value;
		}
	}

	public void Create(Browser browser, Vector2 pos)
	{
		owner = browser;
		position = pos;

		Bounds bounds = GetComponent<SpriteRenderer>().bounds;

		position += new Vector2(bounds.size.x / 2,-bounds.size.y / 2);

		transform.position = new Vector3 (position.x, position.y, -1.0f);

		gameObject.SetActive(true);
	}

	public void Activate()
	{
		foreach(Transform t in transform.parent)
		{
			/*if(t.Equals(transform))
				continue;
				*/
			t.gameObject.SetActive(true);
		}
		
		GetComponent<SpriteRenderer> ().color = Color.white;

		isActive = true;
	}

	public void Deactivate()
	{
		foreach(Transform t in transform.parent)
		{
			if(t.Equals(transform))
				continue;
			t.gameObject.SetActive(false);
		}

		GetComponent<SpriteRenderer> ().color = new Color(0.9f, 0.9f, 0.9f);

		isActive = false;
	}

	public void Close()
	{
		transform.parent.gameObject.SetActive (false);
	}


	public void SetPosition(Vector2 pos)
	{
		position = pos;
		Bounds bounds = GetComponent<SpriteRenderer>().bounds;
		position += new Vector2(bounds.size.x / 2,-bounds.size.y / 2);
		transform.position = Vector3.Lerp (transform.position, new Vector3 (position.x, position.y, -1.0f), Time.fixedDeltaTime * 10.0f);
	}
	// Use this for initialization
	void Start () 
	{
		TabName = transform.parent.gameObject.name;
		Deactivate ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void FixedUpdate()
	{
		//if(isActive)
			//transform.position = Vector3.Lerp (transform.position, new Vector3 (position.x, position.y, -1.0f), Time.fixedDeltaTime * 10.0f);
	}

	void OnMouseDown()
	{
		if(owner == null)
			return;

		owner.ActivateTab (this);
	}
}
