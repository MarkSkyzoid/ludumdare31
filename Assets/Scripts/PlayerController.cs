﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
    public float minSpeed = 5.0f;
	public float maxSpeed = 15.0f;
	public float accelerationRate = 1.0f;
	public float decellerationRate = 5.0f;

	public float jumpForce = 200.0f;
	public float popUpJumpForce = 10.0f;

	private float currentSpeed;

	private bool touchingGround = false;

	public bool inPopup = false;

    void Awake()
    {
		currentSpeed = minSpeed;
    }

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void FixedUpdate()
    {
        Vector3 movement_dir = Vector3.zero;

		movement_dir.x = Time.fixedDeltaTime * minSpeed * Input.GetAxisRaw("Horizontal");
		bool is_moving_horizontally = movement_dir.magnitude > 0.0f;
        
		currentSpeed += Time.fixedDeltaTime * (is_moving_horizontally ? accelerationRate : -decellerationRate);

		currentSpeed = Mathf.Clamp (currentSpeed, minSpeed, maxSpeed);
		transform.position += movement_dir;

		if(Input.GetKey(KeyCode.Space))
		{
			if(touchingGround || inPopup)
			{
				rigidbody2D.AddForce(new Vector2(0.0f, inPopup ? popUpJumpForce : jumpForce));
				touchingGround = false;
			}
		}

//		touchingGround = false;
//		if (rigidbody2D.velocity.y < 0.001f && rigidbody2D.velocity.y > -0.001f)
//			touchingGround = true;
    }

	void LateUpdate()
	{
	}

	void OnCollisionStay2D(Collision2D collision)
	{
		//if(touchingGround) return;

		RaycastHit2D[] hit_info = Physics2D.RaycastAll (transform.position, -Vector2.up);
		foreach(RaycastHit2D hit in hit_info)
		{
			if(hit.collider != null &&  hit.collider.Equals(collision.collider))
			{
				touchingGround = true;
				break;
			}
		}
	}

//	void OnCollisionStay2D(Collision2D collision)
//	{
//		Vector2 collision_normal = Vector2.zero;
//		foreach(ContactPoint2D p in collision.contacts)
//		{
//			//if(p.collider.gameObject.Equals(this.gameObject) == false;) continue;
//			collision_normal += p.normal;
//			collision_normal *= 0.5f;
//		}
//
//		collision_normal = collision.contacts[0].normal;
//		collision_normal.y = transform.position.y - collision.collider.transform.position.y;
//
//		Debug.DrawRay (transform.position, collision_normal, Color.green);
//
//		if (!touchingGround)
//			touchingGround = collision_normal.x == 0.0f && collision_normal.y != 0.0f && collision.relativeVelocity.y <= 0.2f; 
//	}

	void OnCollisionExit2D(Collision2D collision)
	{
		Vector2 collision_normal = Vector2.zero;
		foreach(ContactPoint2D p in collision.contacts)
		{
			//if(p.collider.gameObject.Equals(this.gameObject) == false;) continue;
			collision_normal += p.normal;
			collision_normal *= 0.5f;
		}
		
		//if (touchingGround)
						//touchingGround = false;

	}
}
