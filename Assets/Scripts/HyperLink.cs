﻿using UnityEngine;
using System.Collections;

public class HyperLink : MonoBehaviour 
{
	public Browser owningBrowser = null;
	public Transform tabToOpen = null;
	public Transform[] tabsToOpen;

	public Transform[] tabsToClose;

	// Use this for initialization
	void Start () 
	{
		if (owningBrowser == null)
		{
			GameObject b = GameObject.FindGameObjectWithTag ("Browser");
			owningBrowser = b != null ? b.GetComponent<Browser>() : null;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if(tabToOpen != null && owningBrowser != null && other.gameObject.CompareTag("Player"))
		{
			if(Input.GetKeyDown(KeyCode.E))
			{
				BrowserTab tab_script = tabToOpen.GetComponentInChildren<BrowserTab>();
				
				PopUpVolume popup_script = tabToOpen.GetComponent<PopUpVolume>();

				if(popup_script != null)
					tabToOpen.gameObject.SetActive(true);

				if(tab_script != null)
					owningBrowser.AddTab(tab_script, true);

				foreach(Transform t in tabsToOpen)
				{
					tab_script = t.GetComponentInChildren<BrowserTab>();
					
					popup_script = t.GetComponent<PopUpVolume>();
					
					if(popup_script != null)
						t.gameObject.SetActive(true);
					
					if(tab_script != null)
						owningBrowser.AddTab(tab_script, true);
				}

				foreach(Transform ttc in tabsToClose)
				{
					if(ttc == null) continue;

					tab_script = ttc.GetComponentInChildren<BrowserTab>();

					if(tab_script != null)
						owningBrowser.CloseTab(tab_script);
				}
			}
		}
	}
}
