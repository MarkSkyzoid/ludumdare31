﻿using UnityEngine;
using System.Collections;

public class FullscreenSprite : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		SpriteRenderer sr = GetComponent<SpriteRenderer>();
		if (sr == null) return;
		
		transform.localScale = new Vector3(1,1,1);
		
		float width = sr.sprite.bounds.size.x;
		float height = sr.sprite.bounds.size.y;
		
		float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
		float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

		Vector3 new_localScale = new Vector3 (worldScreenWidth / width, worldScreenHeight / height, 1);
		transform.localScale = new_localScale;
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
