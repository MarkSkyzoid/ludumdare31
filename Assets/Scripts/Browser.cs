﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Browser : MonoBehaviour 
{
	public Transform[] initialTabs;

	public float freezeCountdown = 0.5f; // In seconds.
	public Vector3 topLeftOffset = new Vector3(-1.0f, -1.0f, 0.0f);

	float currentFreezeCountdown = 0.0f;
	bool freezing = false;

	List<BrowserTab> tabs = new List<BrowserTab>();

	Vector3 topLeftPosition;
	Vector3 tabSize = new Vector3(4.0f, 0.0f, 0.0f);

	int currentTab = 0;

	bool initialized = false;

	bool controlMode = false;

	List<BrowserTab> tabsToClose = new List<BrowserTab>();

	// Use this for initialization
	void Start () 
	{
		// Get Top Left Position for placing tabs.
		float cam_half_height = Camera.main.orthographicSize;
		float cam_half_width = Camera.main.aspect * cam_half_height; 
		
		//Bounds bounds = GetComponent<SpriteRenderer>().bounds;
		
		// Set a new vector to the top left of the scene 
		topLeftPosition = new Vector3(-cam_half_width, cam_half_height, 0) + Camera.main.transform.position; 
	}

	void FillInitialTabs()
	{
		int current_tab = 0;
		foreach(Transform tab_transform in initialTabs)
		{
			BrowserTab tab_script = tab_transform.GetComponentInChildren<BrowserTab>();
			if(tab_script == null)
				continue;

			tabs.Add(tab_script);
			Vector3 offset = tabSize * 1.0f * current_tab;
			tab_script.Create(this, topLeftPosition + offset + topLeftOffset);
			++current_tab;
			
			tab_script.Index = current_tab - 1;
			tab_script.Deactivate();
		}

		currentTab = 0;
		tabs [0].Activate ();
	}

	public void AddTab(BrowserTab tab, bool activate = false)
	{
		if(tab == null)
			return;

		tabs.Add (tab);
		tab.Index = (tabs.Count - 1);

		Vector3 offset = tabSize * 1.0f * tab.Index;
		tab.Create(this, topLeftPosition + offset + topLeftOffset);

		if(activate)
			ActivateTab(tab);
	}

	public void ActivateTab(BrowserTab tab)
	{
		if (tab == null || currentTab == tab.Index)
						return;

		tabs [currentTab].Deactivate ();
		currentTab = tab.Index;
		tab.Activate ();
		freezing = true;
		currentFreezeCountdown = freezeCountdown;
		//Time.timeScale = 0.0f;
	}

	public void CloseTab(BrowserTab tab)
	{
		if(tab == null)
			return;

		tabsToClose.Add (tab);
//		tab.Close ();
//		tabs.Remove (tab);
//
//		int index = 0;
//		foreach(BrowserTab t in tabs)
//		{
//			if(t == null) continue;
//
//			t.Index = index;
//			Vector3 offset = topLeftPosition + tabSize * 0.6f * index;
//			t.SetPosition(offset);
//			++index;
//		}
	}
	
	void LateUpdate()
	{
		bool closing_tabs = false;

		BrowserTab current_tab = null;
		if(tabsToClose.Count > 0)
		{
			current_tab = tabs[currentTab];
		}

		foreach(BrowserTab t in tabsToClose)
		{
			if(t == null) continue;
			t.Close();
			tabs.Remove (t);

			closing_tabs = true;
		}

		if(closing_tabs)
		{
			int index = 0;
			foreach(BrowserTab t in tabs)
			{
				if(t == null) continue;
	
				t.Index = index;
				Vector3 offset = topLeftPosition + tabSize * 1 * index + topLeftOffset;
				t.SetPosition(offset);
				++index;
			}

			if(current_tab != null)
				currentTab = current_tab.Index;
		}
	}
	// Update is called once per frame
	void FixedUpdate () 
	{
		Debug.Log ("Control mode: " + controlMode);
		// Quickly switch tabs
		if(Input.GetKey(KeyCode.LeftControl))
		{
			Debug.Log("Control Mode");
			controlMode = true;
			//int tab_index = (currentTab + 1) % tabs.Count;
			//ActivateTab(tabs[tab_index]);
		}

		if(Input.GetKeyUp(KeyCode.LeftControl))
		{
			Debug.Log("Control Mode ended");
			controlMode = false;
			//int tab_index = (currentTab + 1) % tabs.Count;
			//ActivateTab(tabs[tab_index]);
		}

		if(controlMode && Input.GetKey(KeyCode.Tab))
		{
			Debug.Log("Ctrl+TAB");
			int tab_index = (currentTab + 1) % tabs.Count;
			ActivateTab(tabs[tab_index]);
		}

		if(!initialized)
		{
			//Init Tabs.
			FillInitialTabs ();
			initialized = true;
		}

		if(freezing)
		{
			currentFreezeCountdown -= 0.33f; //SORRYY
			if(currentFreezeCountdown <= 0.0f)
			{
				freezing = false;
				currentFreezeCountdown = 0.0f;
				//Time.timeScale = 1.0f;
			}
		}
	}
}
